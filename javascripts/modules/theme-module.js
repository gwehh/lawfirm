AppName.Modules.ThemeModule = (function() {
  //Dependencies
  var core = AppName.Core;

  //////////////////////
  // Private Methods //
  ////////////////////
  var _slickCarousel = function() {
    $('.services-carousel').slick({
      arrows: true,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 567,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    });
    $('.portfolio-carousel').slick({
      arrows: true,
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 4
          }
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 567,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    });
  };

  var _fancyBox = function() {
    $("a.fancybox").fancybox();
  };

  var _stickyHeader = function() {
    var headerHeight = $('.header').outerHeight();
    if ($(window).scrollTop() >= headerHeight) {
      $('.nav-wrapper').addClass('fixed-nav');
    }
    $(document).scroll(function (e) {
      var headerHeight = $('.header').outerHeight();
      if ($(window).scrollTop() >= headerHeight) {
        $('.nav-wrapper').addClass('fixed-nav');
      } else {
        $('.nav-wrapper').removeClass('fixed-nav');
      }
    });
  };

  var _revealOnScroll = function () {
      function revealOnScroll() {
          var $window = $(window),
              win_height_padded = $window.height() * 1.1;
          var scrolled = $window.scrollTop();
          // Showed...
          $('.revealOnScroll:not(.animation-revealScroll)').each(function () {
              var $this = $(this),
                  offsetTop = $this.offset().top;
             if (scrolled + win_height_padded > offsetTop + 100) {
                // check for data attribute 'data-timeout'
                // value should be how long to wait
                  if ($this.data('timeout')) {
                     window.setTimeout(function () {
                          // uses the data-animation attribute to change class
                          $this.addClass('animation-revealScroll ' + $this.data('animation'));
                          console.log($this.data('animation'), 'timeout')
                      }, parseInt($this.data('timeout'), 10));
                  } else {
                      // uses the data-animation attribute to change class
                      $this.addClass('animation-revealScroll ' + $this.data('animation'));
                      console.log($this.data('animation'))
                  }
              }
          });
      }
      window.addEventListener('scroll', function () {
           revealOnScroll();
      });
    };


  /////////////////////
  // Public Methods //
  ///////////////////
  var init = function() {
    _slickCarousel();
    _fancyBox();
    _stickyHeader();
    _revealOnScroll();
  };

  return {
    init: init
  };
})();
